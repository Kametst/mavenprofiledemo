package com.profiles.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProfileUtility {

    public static Properties loadEnvProperties(){
        ProfileUtility getEnvProperties = new ProfileUtility();
        return getEnvProperties.loadPropertiesFile("config.properties");

    }

    public Properties loadPropertiesFile(String filePath) {

        Properties prop = new Properties();
        InputStream iStream = null;
        try {
            // Loading properties file from the classpath
            iStream = this.getClass().getClassLoader()
                    .getResourceAsStream(filePath);
            if(iStream == null){
                throw new IOException("File not found");
            }
            prop.load(iStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
