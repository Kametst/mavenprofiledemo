package com.profiles.test;

import com.profiles.utility.ProfileUtility;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.Properties;

public class ProfileTests {

    private Properties properties = ProfileUtility.loadEnvProperties();

    @Test
    public void RunProfileTest(){

        System.out.println(properties.toString());
        WebDriver driver;
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.get(properties.getProperty("url"));
        driver.quit();
    }


}
