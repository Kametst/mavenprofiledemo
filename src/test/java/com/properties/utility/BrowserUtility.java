package com.properties.utility;

import com.properties.Browser;
import com.properties.Chrome;
import com.properties.Firefox;


public class BrowserUtility {
    public static String browser = System.getProperty("browser");

    public static Browser getbrowser(){
        if (browser.equalsIgnoreCase("chrome")) {
            Chrome chrome = new Chrome();
            return chrome;
        }

        else if (browser.equalsIgnoreCase("Firefox")){
            Firefox firefox = new Firefox();
            return firefox;
        }

        else {
            throw new IllegalArgumentException();
        }
    }
}
