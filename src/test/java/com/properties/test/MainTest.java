package com.properties.test;


import com.properties.Browser;
import com.properties.utility.BrowserUtility;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class MainTest {
    public Browser browser = BrowserUtility.getbrowser();
    WebDriver driver;

    @Test
    public void BrowserTest(){
        driver=browser.getDriver();
        driver.get("https://www.google.co.in/");
    }
}
