package com.properties;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Firefox extends  Browser {
    public Firefox(){
            WebDriver driver;
            WebDriverManager.firefoxdriver().setup();
            driver=new FirefoxDriver();
            setDriver(driver);
        }
    }

