package com.properties;


import lombok.Data;
import org.openqa.selenium.WebDriver;

@Data
public class Browser {
    private WebDriver driver;

}
